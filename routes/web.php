<?php

use Illuminate\Support\Facades\Route;
use app\http\Controllers\EmployeeController

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    
});

Route::get('/pegawai', [EmployeeController::class, 'index'})->name('pegawai');

Route::get('/tambahpegawai', [EmployeeController::class, 'index'})->name('tambahpegawai');
Route::post('/insertdata', [EmployeeController::class, 'index'})->name('insertdata');

Route::get('/tampilkandata/{fd}', [EmployeeController::class, 'tampilkandata'})->name('tampilkandata');
